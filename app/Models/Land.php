<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use App\Models\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Land extends Model
{
    use CrudTrait;
    use HasFactory;
    use SoftDeletes;

    protected $table = "lands";
    protected $fillable = [
        'land_type',
        'width',
        'length',
        'land_number',
        'name',
        'business_name',
        'type_business',
        'building_count',
        'ownername',
        'building_area',
        'building_type',
        'land_area',
        'floor',
        'building_use',
        'style',
        'floor',
        'building',
        'type_construction',
        'user_id',
        'lat',
        'long',
        'image1',
        'image2'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function pagenate(){

        return parent::paginate();
    }

    public function attachments(){
        return $this->hasMany(Attachment::class);
    }
}
