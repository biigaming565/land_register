<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }



    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        $needResponseJson = $request->is('api/*');
        $message = $exception->getMessage();

        if ($exception instanceof AuthenticationException) {

            if ($needResponseJson) {
                return response()->json([
                    'message' => 'Unauthenticated',
                ], 401);
            }
        }

        if ($exception instanceof ValidationException ) {

            if ($needResponseJson) {
                return response()->json([
                    'message' => $message,
                ], 400);
            }
        }

        return parent::render($request, $exception);
    }
}
