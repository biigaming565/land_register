<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\LandRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class LandCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class LandCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Land::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/land');
        CRUD::setEntityNameStrings('land', '',);
        CRUD::enableExportButtons();
        $this->crud->denyAccess(['create', 'update', 'delete']);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        // CRUD::column('User')->lebel('User')->model('App\Models\User')->attribute('first_name');
        // CRUD::setFromDb(); // set columns from db columns.
        CRUD::column('land_number');
        // CRUD::column('width');
        // CRUD::column('length');
        CRUD::column('date');
        CRUD::column('name');
        CRUD::column('business_name');
        CRUD::column('type_business');
        CRUD::column('floor');
        CRUD::column('ownername');
        CRUD::column('building_type');
        CRUD::column('type_construction');
        CRUD::column('building');
        CRUD::column('building_type');
        CRUD::column('building_use');
        CRUD::column('style');
        CRUD::column('image1')->type('base64');
        CRUD::column('image2')->type('base64');
        CRUD::column('building_count');
        /**
         * Columns can be defined using the fluent syntax:
         * - CRUD::column('price')->type('number');
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(LandRequest::class);
        CRUD::setFromDb(); // set fields from db columns.

        /**
         * Fields can be defined using the fluent syntax:
         * - CRUD::field('price')->type('number');
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
