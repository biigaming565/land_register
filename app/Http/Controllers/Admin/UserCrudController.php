<?php

namespace App\Http\Controllers\Admin;

use App\Enums\RoleEnum;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * Class UserCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class UserCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\User::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/user');
        CRUD::setEntityNameStrings('user', 'users');
        $this->crud->denyAccess(['update']);
        CRUD::addButtonFromView('top', 'import', 'import', 'end');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        // CRUD::setFromDb(); // set columns from db columns.

        CRUD::column('first_name');
        CRUD::column('last_name');
        CRUD::column('phone_number');
        CRUD::column([
            'name' => 'role',
            'type' => 'enum',
            // 'model' => RoleEnum::class,
            'label' => 'Role',
        ]);

        /**
         * Columns can be defined using the fluent syntax:
         * - CRUD::column('price')->type('number');
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(UserRequest::class);
        // CRUD::setFromDb(); // set fields from db columns.
        CRUD::field('first_name');
        CRUD::field('last_name');
        CRUD::field('phone_number');
        CRUD::field('password');
        CRUD::field('role')->type('enum')->default('user');

        /**
         * Fields can be defined using the fluent syntax:
         * - CRUD::field('price')->type('number');
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function import(Request $request){
        if($request->hasFile('file')){
            $file = $request->file('file')->getRealPath();
            $extension = $request->file('file')->getClientOriginalExtension();
            try{
                $reader = IOFactory::createReader(ucfirst($extension));
                $spreadsheet = $reader->load($file);
                $data = $spreadsheet->getActiveSheet()->toArray();
                $data = array_slice($data, 1);
                foreach($data as $user){
                    info($user);
                    User::create([
                        'first_name' => $user[0],
                        'last_name' => $user[1],
                        'phone_number' => $this->ConvertPhoneToStandart($user[2]),
                        'password' => bcrypt($user[3] ?? '123456')
                    ]);
                    DB::commit();
                    info($user);
                }
                return $this->sendSuccess([], 'Imported Sucessful');
            }catch(\Exception $e){
                $this->sendError($e->getMessage(), 400);
            }
        }
    }
    protected function sendSuccess($data,$message = null){

        if ($data instanceof JsonResource) {
            return $data->additional(["success" => true]);
        }

        return response()->json([
            "data" => $data,
            "message" => $message,
            "success" => true
        ]);
    }

    protected function sendError($message,$httpCode){
        $message = is_array($message) ? $message : ["message" => $message];
        $errors = array_merge($message,[
            "success" => false,
        ]);

        return response()->json($errors,$httpCode);
    }
    public function ConvertPhoneToStandart($phone)
    {
        $countryCode = '+855';
        if (str_starts_with($phone, '0')) {
            $phone = $countryCode . substr($phone, 1);
        } else if (str_starts_with($phone, '+')) {
            // return $phone;
        } else {
            $phone = (int)$phone;
            $phone = sprintf('+855%d', $phone);
        }
        return $phone;
    }
    public function ConvertItBack($phone)
    {
        return '0' . substr($phone, 4);
    }
}
