<?php

namespace App\Http\Controllers\API;

use App\Models\Land;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Resources\LandResource;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\PaginationAccountResource;
use App\Models\Attachment;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Nette\Schema\Schema;

class LandController extends ApiBaseController
{
    public function create(Request $request){
        try {
            Log::debug("request_all");
            Log::debug($request->all());
            $request->validate([
                'land_type' => 'required|string',
                'width' => 'required|numeric',
                'length' => 'required|numeric',
                'name' => 'nullable|string',
                'business_name' => 'nullable|string',
                'type_business' => 'nullable|string',
                'building_count' => 'nullable|integer',
                'land_number' => 'nullable|string|unique:lands,land_number',
                'ownername' => 'nullable|string',
                'building_use' => 'nullable|string',
                'building' => 'nullable|string',
                'building_type' => 'nullable|string',
                'building_area' => 'nullable|string',
                'floor' => 'nullable|integer',
                'building_use' => 'nullable|string',
                'type_construction' => 'nullable|string',
                'style' => 'nullable|string',
                'image1'  => 'nullable|file|image|max:1024',
                'image2'  => 'nullable|file|image|max:1024',
                'lat' => 'nullable|string',
                'long' => 'nullable|string',
            ]);

            $params = $request->all();
            $params['width'] = (float) $params['width'];
            $params['length'] = (float) $params['length'];
            Log::debug("params");
            Log::debug($params);
            $land = Land::create($params);
            $land->image1 = $this->turnToBase64($request, $land, 'image1') ?? '';
            $land->image2 = $this->turnToBase64($request, $land, 'image2') ?? '';
            $land->user_id = auth()->user()->id;
            $land->save();
            return $this->sendSuccess(new LandResource($land), 'Land created successful');

        } catch (\Throwable $th) {
            log::debug('create_land_error');
            log::debug($th);
            throw $th;
            // return $this->sendError($th->validator->errors()->first(),400);
        }
    }

    public function index(Request $request){
        $limit = $request->input('limit') ?? 5;
        $currentPage = $request->input('page') ?? 1;
        $user = auth()->user();
        $all_columns = [
            'lands.id',
            'lands.land_type',
            'lands.width',
            'lands.length',
            'lands.land_number',
            'lands.name',
            'lands.business_name',
            'lands.type_business',
            'lands.building_count',
            'lands.ownername',
            'lands.building_area',
            'lands.building_type',
            'lands.floor',
            'lands.building_use',
            'lands.style',
            'lands.floor',
            'lands.building',
            'lands.type_construction',
            'lands.user_id',
            'lands.lat',
            'lands.long',

            DB::raw("CONCAT('".asset('/')."', REPLACE((SELECT paths FROM attachments WHERE attachments.land_id = lands.id AND attachments.name = 'image1' LIMIT 1), 'public', 'storage')) AS image1"),
            DB::raw("CONCAT('".asset('/')."', REPLACE((SELECT paths FROM attachments WHERE attachments.land_id = lands.id AND attachments.name = 'image2' LIMIT 1), 'public', 'storage')) AS image2")
        ];
        $lands = Land::query()
            // ->leftJoin('attachments', 'attachments.land_id', 'land.id')
            ->select($all_columns)
            // ->addSelect(
                // DB::raw("CONCAT('". asset('/') ."', REPLACE(attachments.paths, 'public', 'storage')) AS image1"),
                // DB::raw("CONCAT('". asset('/') ."', REPLACE(attachments.paths, 'public', 'storage')) AS image2"),
            // )
            // ->where('lands.id', 6)
            ->orderBy('lands.id', 'DESC')
            ->paginate($limit);
        // $lands->map(function ($land){
        //     $attachment1 = Attachment::where('land_id', $land->id)->where('name', 'image1')->first();
        //     $attachment2 = Attachment::where('land_id', $land->id)->where('name', 'image2')->first();
        //     if($attachment1){
        //         $land->image1 = asset(str_replace('public', 'storage', $attachment1->paths));
        //     }
        //     if($attachment2){
        //         $land->image2 = asset(str_replace('public', 'storage', $attachment2->paths));
        //     }
        //     return $land;
        // });
        return $this->sendSuccess($lands);
        // return $this->sendSuccess($lands->paginate($limit));
    }

    public function turnToBase64(Request $request, Land $land, $fileNameOG='file'){
        $file = $request->file($fileNameOG);
        $ext = $file->getClientOriginalExtension() ?? 'jpg';
        $fileName = Str::random(10) . '.' . $ext; // Define your custom filename here
        // $link = asset('storage/temp/' . $fileName);
        $new_file = 'public/temp/' . $fileName;

        Storage::put($new_file, file_get_contents($file));
        $attachment = Attachment::create([
            'name' => $fileNameOG,
            'paths' => $new_file,
            'file' => $fileName,
            'ext' => $ext,
            'land_id' => $land->id
        ]);
        return base64_encode(file_get_contents($file));
    }
}
