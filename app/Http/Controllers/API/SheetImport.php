<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\IOFactory;
use App\Models\User;

class SheetImport extends ApiBaseController
{
    public function import(Request $request){
        if($request->hasFile('file')){
            $file = $request->file('file')->getRealPath();
            $extension = $request->file('file')->getClientOriginalExtension();
            try{
                $reader = IOFactory::createReader(ucfirst($extension));
                $spreadsheet = $reader->load($file);
                $data = $spreadsheet->getActiveSheet()->toArray();
                $data = array_slice($data, 1);
                foreach($data as $user){
                    User::create([
                        'first_name' => $user[0],
                        'last_name' => $user[1],
                        'phone_number' => $this->ConvertPhoneToStandart($user[2]),
                        'password' => bcrypt($user[3] ?? '54321')
                    ]);
                }
                // return $this->sendSuccess($data);
                return $this->sendSuccess([], 'Imported Sucessful');
            }catch(\Throwable $e){
                $this->sendError($e->getMessage(), 400);
            }
        }
    }
    public function ConvertPhoneToStandart($phone)
    {
        $countryCode = '+855';
        if (str_starts_with($phone, '0')) {
            $phone = $countryCode . substr($phone, 1);
        } else if (str_starts_with($phone, '+')) {
            // return $phone;
        } else {
            $phone = (int)$phone;
            $phone = sprintf('+855%d', $phone);
        }
        return $phone;
    }
    public function ConvertItBack($phone)
    {
        return '0' . substr($phone, 4);
    }
}
