<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends ApiBaseController
{
    public function __construct()
    {
        Auth::setDefaultDriver("api");
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile() {
        return $this->sendSuccess(new UserResource(auth()->user()));
    }

    public function login(Request $request)
    {

        try {

            $request->validate([
                'phone_number' => 'required',
                'password' => 'required|string',
            ]);

        } catch (\Throwable $th) {
            return $this->sendError($th->validator->errors()->first(),400);
        }


        $credentials = $request->only('phone_number', 'password');

        $credentials['phone_number'] = $this->ConvertPhoneToStandart($credentials['phone_number']);

        $token = Auth::attempt($credentials);

        if (!$token) {
            return $this->sendError("Unauthorized",401);
        }

        $user = auth()->user();

        return $this->sendSuccess($this->createNewToken($token));
    }

    public function register(Request $request)
    {
        try {
            $request->validate([
                'phone_number' => 'required',
                'first_name' => 'required|string',
                'last_name' => 'required|string',
                'password' => 'required|string',
            ]);
            $phone = ['phone_number' => $this->ConvertPhoneToStandart($request->phone_number)];
            Validator::validate($phone, [
                'phone_number' => 'unique:users,phone_number'
            ]);
        } catch (\Throwable $th) {
            return $this->sendError($th->validator->errors()->first(),400);
        }
        $credentials = $request->only('phone_number', 'first_name', 'last_name', 'password');
        // $credentials['password'] = bcrypt($credentials['password']);
        $credentials['phone_number'] = $this->ConvertPhoneToStandart($credentials['phone_number']);
        User::create($credentials);
        return $this->sendSuccess([],"Successfully Registered out");
    }


    public function logout()
    {
        Auth::logout();
        return $this->sendSuccess([],"Successfully logged out");
    }

    public function refresh()
    {
        return $this->sendSuccess($this->createNewToken(auth()->refresh()));
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token){

        return [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            "user" => new UserResource(auth()->user())
        ];
    }

    public function ConvertPhoneToStandart($phone)
    {
        $countryCode = '+855';
        if (str_starts_with($phone, '0')) {
            $phone = $countryCode . substr($phone, 1);
        } else if (str_starts_with($phone, '+')) {
            // return $phone;
        } else {
            $phone = (int)$phone;
            $phone = sprintf('+855%d', $phone);
        }
        return $phone;
    }
    public function ConvertItBack($phone)
    {
        return '0' . substr($phone, 4);
    }

}
