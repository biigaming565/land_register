<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Termwind\Components\Dd;

class ApiBaseController extends Controller
{

    public function __construct()
    {
        Auth::setDefaultDriver("api");
    }

    protected function sendSuccess($data,$message = null){

        // if ($data instanceof JsonResource) {
        //     $data = ($data->additional(["success" => true]));
        //     return $data->toResponse(new Request());
        // }
        return response()->json( [
                "data" => $data,
                "message" => $message,
                "success" => true
            ]
        );
    }

    protected function sendError($message,$httpCode){
        $message = is_array($message) ? $message : ["message" => $message];
        $errors = array_merge($message,[
            "success" => false,
        ]);

        return response()->json($errors,$httpCode);
    }

}
