<?php

namespace App\Http\Controllers;

use Backpack\CRUD\app\Library\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class AuthController extends Controller
{
    protected ?string $loginPath = null;
    protected ?string $redirectTo = null;
    protected ?string $redirectAfterLogout = null;

    protected $data = []; // the information we send to the view

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers {
        logout as defaultLogout;
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $guard = backpack_guard_name();

        $this->middleware("guest:$guard", ['except' => 'logout']);

        // ----------------------------------
        // Use the admin prefix in all routes
        // ----------------------------------

        // If not logged in redirect here.
        $this->loginPath ??= backpack_url('login');

        // Redirect here after successful login.
        $this->redirectTo ??= backpack_url('dashboard');

        // Redirect here after logout.
        $this->redirectAfterLogout ??= backpack_url('login');
    }

    /**
     * Return custom username for authentication.
     *
     * @return string
     */
    public function username()
    {
        return backpack_authentication_column();
    }

    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        return redirect($this->redirectAfterLogout);
    }

    /**
     * Get the guard to be used during logout.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return backpack_auth();
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            if (config('backpack.base.setup_email_verification_routes', false)) {
                return $this->logoutIfEmailNotVerified($request);
            }

            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    protected function credentials(Request $request)
    {
        $nice =  $request->only($this->username(), 'password');
        $nice[$this->username()] = $this->ConvertPhoneToStandart($nice[$this->username()]);
        return $nice;
    }

    public function ConvertPhoneToStandart($phone)
    {
        $countryCode = '+855';
        if (str_starts_with($phone, '0')) {
            $phone = $countryCode . substr($phone, 1);
        } else if (str_starts_with($phone, '+')) {
            // return $phone;
        } else {
            $phone = (int)$phone;
            $phone = sprintf('+855%d', $phone);
        }
        return $phone;
    }
    public function ConvertItBack($phone)
    {
        return '0' . substr($phone, 4);
    }

}
