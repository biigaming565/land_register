<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class LandResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'land_type' => $this->land_type,
            'width' => $this->width,
            'length' => $this->length,
            'name' => $this->name,
            'business_name' => $this->business_name,
            'type_business' => $this->type_business,
            'land_number' => $this->land_number,
            'ownername' => $this->ownername,
            'land_area' => $this->land_area,
            'building_area' => $this->building_area,
            'building_type' => $this->building_type,
            'building_count' => $this->building_count,
            'floor' => $this->floor,
            'building' => $this->building,
            'building_use' => $this->building_use,
            'type_construction' => $this->type_construction,
            'style' => $this->style,
            'user' => $this->when($this->user_id,new UserResource($this->user)),
            'lat' => $this->lat,
            'long' => $this->long,
            'image1' => $this->image1,
            'image2' => $this->image2
        ];
    }
}
