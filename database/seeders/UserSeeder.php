<?php

namespace Database\Seeders;

use App\Enums\RoleEnum;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        if (User::count() == 0) {

            User::create([
                'first_name'           => 'admin',
                'last_name'          => 'admin',
                'phone_number'   => '+85510101010',
                'password'       => bcrypt('6buzPH2A6XJUTHvOX6xzQr'),
                'remember_token' => Str::random(60),
                'role'           => RoleEnum::ADMIN,
            ]);
        }
    }
}
