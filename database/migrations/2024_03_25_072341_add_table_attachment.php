<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::dropIfExists('attachments');
        Schema::create('attachments', function(Blueprint $table){
            $table->id();
            $table->string('name')->nullable();
            $table->string('paths')->nullable();
            $table->string('file')->nullable();
            $table->string('ext')->nullable();
            $table->unsignedBigInteger('land_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
