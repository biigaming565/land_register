<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::dropIfExists('lands');
        Schema::create('lands', function (Blueprint $table) {
            $table->id();
            $table->string('land_type');
            $table->integer('width');
            $table->integer('length');

            $table->string('land_number')->unique()->nullable();
            $table->string('name')->nullable();
            // $table->string('land_area')->nullable();
            $table->string('business_name')->nullable();
            $table->string('type_bussiness')->nullable();

            $table->string('bL_count')->nullable();
            $table->string('building_area')->nullable();
            $table->integer('floor')->nullable();

            $table->string('street')->nullable();
            $table->string('village')->nullable();
            $table->string('sangkat')->nullable();
            $table->string('province')->nullable();

            $table->string('type_roof')->nullable();

            $table->string('bl_age')->nullable();

            $table->string('bl_condition')->nullable();

            $table->string('type_construction')->nullable();

            $table->string('phone')->nullable();
            $table->string('ownername')->nullable();

            $table->string('building')->nullable();
            $table->string('building_type')->nullable();
            $table->string('building_use')->nullable();
            $table->string('style')->nullable();
            $table->string('price_building')->nullable();
            $table->string('build_year')->nullable();


            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->string('lat')->nullable();
            $table->string('long')->nullable();

            $table->longText('image1')->nullable();
            $table->longText('image2')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('lands');
    }
};
