<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('lands', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->string('land_number');
            $table->string('owner_name');
            $table->string('land_area');
            $table->string('land_use');

            $table->string('house_type');
            $table->string('total_flat');
            $table->string('house_area');
            $table->string('totla_floor');
            $table->string('house_use');
            $table->string('construct_type');
            $table->string('house_mode');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('table_lands');
    }
};
