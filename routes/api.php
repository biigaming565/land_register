<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\LandController;
use App\Http\Controllers\API\SheetImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(["prefix" => "v1"], function (){
    Route::group(["prefix" => "auth"],function($router){
        $router->post('register', [AuthController::class,"register"]);
        $router->post("login",[AuthController::class,"login"]);
        $router->post("logout",[AuthController::class,"logout"]);
        $router->post("refresh",[AuthController::class,"refresh"]);
        $router->get("user-profile",[AuthController::class,"userProfile"]);
    });

    Route::group(["prefix" => "land", "middleware" => "auth:api"],function($router){
        $router->get('/', [LandController::class, "index"]);
        $router->post('/', [LandController::class, "create"]);
    });

    //map
    // Route::group(["prefix" => "map", "middleware" => "auth:api"],function($router){
    //     $router->get('/', [LandController::class, "index"]);
    //     $router->post('/', [LandController::class, "create"]);
    // });

    Route::post('/users/import', [SheetImport::class, "import"]);
});
