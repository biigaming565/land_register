<input type="file" id="fileupload">
<a href="javascript:void(0)" class="btn btn-primary" onclick="importTransaction(this)" id="openUploadModal" data-route="{{ url($crud->route.'/import') }}" class="btn btn-sm btn-link" data-button-type="importog">
<span class="ladda-label"><i class="la la-plus"></i> Import {{ $crud->entity_name }}</span>
</a>

<style>
.modal {
  display: none;
  position: fixed;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5);
}

.modal-content {
  background-color: #fff;
  margin: 15% auto;
  padding: 20px;
  border: 1px solid #888;
  width: 40%;
}

.close-btn {
  color: #aaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close-btn:hover,
.close-btn:focus {
  color: black;
  text-decoration: none;
  cursor: pointer;
}
</style>

@push('after_scripts')
<script>
    if (typeof importTransaction != 'function') {
      $("[data-button-type=import]").unbind('click');
      function importTransaction(button) {
          var button = $(button);
          var route = button.attr('data-route');
          const fileInput = $('#fileupload');
          const files = fileInput.prop('files');
          if(files.length == 0){
            new Noty({
              text: "Please select a file before uploading.",
              type: "warning"
            });
            return;
          };
          const firstFile = files[0];
          var route = $(this).attr('data-route');
          let formData = new FormData();
          formData.append('file', files[0]);

          $.ajax({
              url: '/admin/user/import',
              type: 'POST',
              accepts: 'application/json',
              processData: false,
              contentType: false,
              data: formData,
              success: function(result) {
                  // Show an alert with the result
                  console.log(result,route);
                  new Noty({
                      text: "Some Tx had been imported",
                      type: "success"
                  }).show();

                  // Hide the modal, if any
                  $('.modal').modal('hide');

                  crud.table.ajax.reload();
              },
              error: function(result) {
                  console.log(result);
                  new Noty({
                      text: "The new entry could not be created. Please try again.",
                      type: "warning"
                  }).show();
              }
          });
      }
    }

</script>
@endpush
